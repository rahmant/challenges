<?php

/**
 * Takes associative array as an argument and returns new array that has locations as key containing location residents
 * in an array.
 *
 * @param array $personLocations
 *
 * @return array
 */
function formatArrayByLocation( array $personLocations ): array {
	$city = array();
	$result = array();

	foreach ($personLocations as $key => $val)
	{
		if (!in_array($val, $city))
		{
			array_push($city, $val);
		}
	}
	foreach ($city as $ct)
	{
		$result[$ct] = array();
		foreach ($personLocations as $name => $city_val)
		{
			if ($city_val === $ct){
				array_push($result[$ct], $name);
			}
		}
	}
	return $result;
}

$personLocations = [
	'Ilmari Ikonen'      => 'Myllykoski',
	'Villiam Sundqvist'  => 'Rauma',
	'Kusti Eerola'       => 'Helsinki',
	'Tarvo Rantanen'     => 'Joensuu',
	'Isto Palander'      => 'Helsinki',
	'Eriika Haapalainen' => 'Joensuu',
];

print_r( formatArrayByLocation( $personLocations ) );
