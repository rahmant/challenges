# Sorting challenge

In this task you're given an array of people names and locations. Your task is to implement the formatArrayByLocation
function so that it returns an array where key is the location and location has array of people names.

The output should look something like this:
```php
Array
(
    [Rauma] => Array
        (
            [0] => Villiam Sundqvist
        )
    [Joensuu] => Array
        (
            [0] => Tarvo Rantanen
            [1] => Eriika Haapalainen
        )
)
```
