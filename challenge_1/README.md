# Async challenge

This program is supposed to ping selusta.com every now and then to make sure the website is up and running.
However, currently running the program results to following stack trace:

```
PHP Fatal error:  Uncaught Error: Object of class Amp\Coroutine could not be converted to string in app.php:25
Stack trace:
0 vendor\amphp\amp\lib\Loop\NativeDriver.php(142): {closure}()
1 vendor\amphp\amp\lib\Loop\Driver.php(138): Amp\Loop\NativeDriver->dispatch()
2 vendor\amphp\amp\lib\Loop\Driver.php(72): Amp\Loop\Driver->tick()
3 vendor\amphp\amp\lib\Loop.php(95): Amp\Loop\Driver->run()
4 app.php(31): Amp\Loop::run()
5 {main}
  thrown in app.php on line 25
```

The task here is to fix the program so that it works correctly. The program output should look something like this:
```
string(24) "The pinger result is 200"
string(24) "The pinger result is 200"
string(24) "The pinger result is 200"
string(24) "The pinger result is 200"
```

Run the program by first installing composer dependencies `composer install` and then start the program by running `composer start` or `php app.php`
