<?php


use Amp\Http\Client\HttpClientBuilder;
use Amp\Http\Client\Request;
use Amp\Promise;

class Pinger {
	/**
	 * Pings desired url and returns HTTP status code integer depending on ping result.
	 *
	 * @param string $url
	 *
	 * @return Promise<integer>
	 */
	public static function ping( string $url ): Promise {
		return Amp\call( function () use ( $url ) {
			try {
				$client = HttpClientBuilder::buildDefault();

				$response = yield $client->request( new Request( $url ) );

				return $response->getStatus();
			} catch ( \Exception $exception ) {
				return 0;
			}
		} );
	}
}
